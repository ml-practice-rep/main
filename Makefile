docker-upd:
	(docker stop ods-mlops-con&>/dev/null) && { docker rm ods-mlops-con; } || { echo "no container ods-mlops-con"; }
	docker run --name ods-mlops-con -d -p 8000:8000 ods-mlops
docker-build:
	docker build -t ods-mlops .
docker-re-build-a-run-d:
	docker build -t ods-mlops .
	(docker stop ods-mlops-con&>/dev/null) && { docker rm ods-mlops-con; } || { echo "no container ods-mlops-con"; }
	docker run --name ods-mlops-con -d -p 8000:8000 ods-mlops
docker-logs:
	docker logs ods-mlops-con
vscode-freeze-extensions:
	code --list-extensions > vscodeextensions.txt
vscode-install-extensions:
	cat vscodeextensions.txt | xargs -L 1 code --install-extension
