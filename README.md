# ods-MLOps
Проект по курсы ods-MLOps


## Build & run

### Local

#### For Docker
##### Make
```sh
make docker-re-build-a-run-d
```
##### sh
```sh
docker build -t ods-mlops .
docker run --name ods-mlops-con -d -p 8000:8000 ods-mlops
```

#### For local
first install
```sh
curl -sSL https://pdm-project.org/install-pdm.py | python3 -
pdm venv create
pdm install
```
