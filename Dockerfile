FROM mambaorg/micromamba

# USER root
WORKDIR /app

COPY . .
USER root
RUN chown -R mambauser:mambauser /app
USER mambauser

RUN micromamba create -f env.yml
RUN micromamba run -n env_research pdm config python.use_venv False
RUN micromamba run -n env_research pdm install --check -g -p /app

# RUN curl -sSL https://pdm-project.org/install-pdm.py | python3 -
# ENV PATH="/root/.local/bin:${PATH}"
# RUN pdm config python.use_venv False
# RUN pdm install --check -g -p /app

# ENTRYPOINT [ "micromamba", "run", "-n", "env_research", "python", "src/ods_mlops/ex.py" ]
