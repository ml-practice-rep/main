# Коллективное участие в проекте.

$`\textcolor{red}{\text{Этап Формирирование (могут быть изменния, могут быть добавление).}}`$

## Простой чек-лист использования

- если скачали в первый раз, выполните [install](#install)
- ведеться поход методологии CF-DS ["centralized-flow"](#centralized-flow)
- [Манифест](#Manifest)
- и стандартный процесс Гитлаба - Мердж-реквесты и т.д.
- [Python linter & formatter](#Python-linter-&-formatter)
- [C++ formatter](#C++)
- Требование к исследованиям

## install

```
python3 -m venv venv
source venv/bin/activate
pip install poetry
poetry install
pre-commit install

```

## Centralized-Flow
Методология опираться на `Data Science Lifecycle Process`.
Централизованного флов (*читать в подвале) в том что только один главный репозиторий существует и он в корневой группе. Только главный репозиторий содержит дев и прод код, а так же ветки `feature` и `research` для продукта (методология git flow). Также рекомендуется имя главного репозитория "Main". Все исследования протекают в других соседних репозиториях этой же группы или подгрупп, созданных после одобрение мердж-реквеста в ветку `research` Манифеста.  Для создание Манифеста нужно создавать новую ветку (с индификатором проекта)внести свое исследование в каталог `research/README.md` ( №, индефикатор, имя проекта, статус, участники, краткое описание) и так же нужно создавать каталог в папки `research/exemple/` (exemple - индификатор проекта) папка должна содержать файл `README.md` - описание проекта, создаеться мердж-реквест, После одобрение ветка вливаеться в `research` и удаляется. После окончания исследования, создается снова ветка с индификатором проекта, и в папку вашего Манифеста `research/exemple/` кладутся артефакты, выводы, отчет и тд..  Могут быть созданы отдельные подгруппы, для разных команд, именные подгруппы, так же закрытые группы для независимых слодовний (с отдельный доступ).

[![flow](doc/flow.png)]()
**ветвление в git**

[miro](https://miro.com/app/board/uXjVKae260U=/?share_link_id=644438816189)

```
.
├── gitlab-ci/
├── doc/
├── src/
├── test/
├── docker/
├── research/
│   ├── one/
│   │   ├── result.csv
│   │   └── README.md
│   ├── two/
│   │   ├── data.sqlite
│   │   ├── result.csv
│   │   └── README.md
│   ├── three/
│   │   ├── result.ipynb
│   │   └── README.md
│   └── README.md
├── .gitignore
├── .dockerignore
├── .gitlab-ci.yml
├── contributing.md
├── Dockerfile
├── Makefile
├── Makefile
├── file
└── README.md
```
**структура проекта**

Возможны независимые исследование, которое не требует внесение манифеста и одобрение мердж-реквеста, независимые исследование всегда проиходит в именной группе или группе команды, где есть прова на создание нового репозитория или группы. После окончание исследование можно внести уже манифест с полученными данными.
* индификаторы проектов должны совпадать с именем папки, так как после будет создан одноименный репозитории
* идентификаторы проектов должны быть уникальными


## Manifest
Манифест - это письменное изложение идея/гипотеза/теория или группы их. В него входит реестр `./research/README.md`, тезис - аннотация кратко сформулированные основные положения, главные мысли идеи/гипотезы/теории ( `research/exemple/README.md`,  где `/exemple/` это папка с название проекта).
1. Создается ответвление ветка от `research` с именем `exemple` и с манифестом, запрос на мардж реквест для апрува исследования. После апрува, ветка `/exemple/` будет удалена.
2. Когда исследование будет  закончена,  создается снова ветка с именем `/exemple/` где в католог `research/exemple/` добавляются результаты. а в реестре `./research/README.md` изменяем статус иследовния.

```
.
...
├── research/
│   ├── one/
│   │   ├── result.csv
│   │   └── README.md
│   ├── two/
│   │   ├── data.sqlite
│   │   ├── result.csv
│   │   └── README.md
│   ├── three/
│   │   ├── result.ipynb
│   │   └── README.md
│   └── README.md
...
```
иерархия каталогов в папке `research`

[![flow](doc/branch-research.png)]()

ветвление от ветки `research`

## VS code settings

### Settings

```json
{
    "notebook.formatOnSave.enabled": true,
    "notebook.codeActionsOnSave": {
        "notebook.cource.fixAll":"explicit",
        "notebook.cource.organizeImports" :"explicit"
    } ,
    "[python]": {
        "editor.defaultFormatter": "charliermarsh.ruff",
        "editor.codeActionsOnSave": {
            "source.fixAll": "explicit",
            "source.organizeImports": "explicit"
        }
    }
}

```

### Extensions

vscode instsll extensions

```bash
make vscode-install-extensions:
```
if not Make run bash

```bash
cat vscodeextensions.txt | xargs -L 1 code --install-extension
```
<details>
<summary>Все расширения</summary>

* charliermarsh.ruff
* eamodio.gitlens
* ms-azuretools.vscode-docker
* ms-python.debugpy
* ms-python.python
* ms-python.vscode-pylance
* ms-toolsai.jupyter
* ms-toolsai.jupyter-keymap
* ms-toolsai.jupyter-renderers
* ms-toolsai.vscode-jupyter-cell-tags
* ms-toolsai.vscode-jupyter-slideshow
* ms-vscode-remote.remote-containers
* ms-vscode.cmake-tools
* ms-vscode.cpptools
* ms-vscode.cpptools-extension-pack
* ms-vscode.cpptools-themes
* ms-vscode.makefile-tools
* njpwerner.autodocstring
* twxs.cmake

</details>


## Python-linter-&-formatter

Для Python линтера/форматтера используется ruff.

- **auto**  после прервой установки  ыеести команду `pre-commit install`, перед каждым коммитом или пушем будет автоматически проводиться исправление ошибок, найденных линтером и форматирование файла.

- **manual** перед каждым коммитом нужно вводить команду `ruff check --fix` и `ruff format` навыбор для всего проекта или для файла/лов.

## C++
* Правила форматировантие кода лежат в `.clang-format`
* cmake >= 3.22
* gcc >= 11.04


## Требование к исследованиям

### Требования к репозиторию исследования

Репозиторий `example-project` содержит в себе  шаблон/заготовку для исследование/исследователя, описаны требования по код(coding convention).

1. Должны находиться на уровне группы основной или ниже (то есть в подгруппе группы основной).
2. Вес исследование должны быть форком репозитория `example-project`.
3. Пул-реквест не создается, исследование остаться в своем репозитории.
4. При окончании исследование (когда статус в реестре изменён на -  `завершен 🟢`) изменения в репозитории **запрещены** (это нужно для максимальной воспроизводимости/повторяемости исследование)
5. Если требуется внести изменения создается новый манифест

### Требования к исследованию

Все исследования в данной команде проводятся на языковых стеках Python и/или С++. Конфиг файлы линтеров и форматеров нельзя изменять. Все исследование должно проводиться в докере  если это невозможно, должно быть описана максимально этапы для воспроизводимости проекта.


Для добавление нового языка нужно:
1. подготовить манифест
2. сделать форк `example-project` и добавить требование под новый язык
3. провести исследование
4. закончить его.

После доказательства состоятельности языка будет добавлен в стек

## Подвал
У меня вроде получилось что то свое выдумать, на основе существующих решений,  не знаю живучее  это или нет. Предлагаю свой вариант.
